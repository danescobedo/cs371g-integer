// ---------------
// TestInteger.c++
// ---------------

// --------
// includes
// --------

#include "gtest/gtest.h"

#include "Integer.hpp"

using namespace std;

// ----
// plus
// ----

TEST(IntegerFixture, solve) {
    Integer<char> x = 0;
    Integer<char> y = 0;
    Integer<char> z = x + y;
    ASSERT_EQ(z, 0);}
